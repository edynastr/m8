<?php
/**
 * Created by PhpStorm.
 * User: nastr
 * Date: 13/03/2019
 * Time: 16:20
 */

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PageController extends Controller
{

    public function homeAction(){
        return $this->render("AppBundle:Home:index.html.twig");

    }

}